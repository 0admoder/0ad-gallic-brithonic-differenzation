        var colores, ctxColores, colores;
        var canvasVideo, ctxVideo, video;
        var canvasControles, ctxControles, controles;
        var xPoint, yPoint, down;
        
        window.addEventListener('load', function () {
            if (!hayCanvas()) {
                alert("Tu navegador no soporta canvas");
            }
            video = document.createElement("video");
            if (video == null) {
                alert("Tu navegador no soporta video");
            }
          
          
            colores = document.getElementById("colores")
            canvasVideo = document.getElementById("canvasVideo");
            canvasControles = document.getElementById("canvasControles");
            ctxVideo = canvasVideo.getContext("2d");
            ctxControles = canvasControles.getContext("2d");
            video.src = "https://dl.dropboxusercontent.com/u/101275290/emoldex.ogv";
            video.addEventListener('loadedmetadata', function () {
                canvasVideo.width = canvasControles.width =
                 333;
                canvasVideo.height = canvasControles.height =
                 333;
            });
            video.addEventListener('canplay', function () {
                setInterval(paintVideo, 33);
                setInterval(paintControles, 37);
                setInterval(paintColores, 17);           
            }
            );
            
            var play = new Boton(pintarPlay, playPause);
            var bm = new Boton(pintarBm, accionBm);
            var mute = new Boton(pintarMute, muted);
            var cd = new Boton(pintarCd, accionCd);
            var bn = new Boton(pintarBn, accionBn);
            var an = new Boton(pintarAn, accionAn);
            var gr = new Boton(pintarGr, accionGr);
            var fi = new Boton(pintarFi, accionFi);
            controles = [play, mute, bn, bm, cd, an, gr, fi];
             var play = new Boton(pintarPlay, playPause);
            var bm = new Boton(pintarBm, accionBm);
            var mute = new Boton(pintarMute, muted);
            var cd = new Boton(pintarCd, accionCd);
            var bn = new Boton(pintarBn, accionBn);
            var an = new Boton(pintarAn, accionAn);
            var gr = new Boton(pintarGr, accionGr);
            controles = [play, mute, bn, bm, cd, an, gr, fi];
            
            canvasControles.addEventListener("mousedown", mouseDown);
            canvasControles.addEventListener("mouseup", mouseUp);
            canvasControles.addEventListener("mouseout", mouseUp);
            canvasControles.addEventListener("mousemove", movePoint);
        });
        
        function paintVideo() {
            ctxVideo.drawImage(video, 0, 0,
                canvasVideo.width, canvasVideo.height);
        }
        
        function paintControles() {
            ctxControles.clearRect(0, 0,
                canvasControles.width, canvasControles.height);
            for (var i=0; i<controles.length; i++) {
                controles[i].mover();
                if (down && 
                    (controles[i].x-xPoint)*(controles[i].x-xPoint)+
                    (controles[i].y-yPoint)*(controles[i].y-yPoint)
                    <= controles[i].r*controles[i].r)
                {
                        controles[i].accion();
                        down = false;
                }
                controles[i].estilo(ctxControles);
            }
        }
   function paintVideo() {
            ctxVideo.drawImage(video, 0, 0,
                canvasVideo.width, canvasVideo.height);
        }
        
        function paintColores() {
            ctxColores.clearRect(0, 0,
                canvasColores.width, canvasColores.height);
            for (var i=0; i<colores.length; i++) {
                controles[i].mover();
                if (down && 
                    (colores[i].x-xPoint)*(colores[i].x-xPoint)+
                    (colores[i].y-yPoint)*(colores[i].y-yPoint)
                    <= colores[i].r*colores[i].r)
                {
                        colores[i].accion();
                        down = false;
                }
                colores[i].estilo(ctxControles);
            }
        }
                
        
        function mouseDown(e) {
            down = true;
            movePoint(e);
        }
        
        function mouseUp(e) {
            down = false;
        }
        
        function movePoint(e) {
            var ie = navigator.userAgent.toLowerCase()
                .indexOf('msie')!=-1;
            if (ie) {
                e = window.event;
                e.pageX = e.clientX+window.pageXOffset;
                e.pageY = e.clientY+window.pageYOffset;
            }
            xPoint = e.pageX-canvasControles.offsetLeft;
            yPoint = e.pageY-canvasControles.offsetTop;
        }
        
        function hayCanvas() {
            var elem = document.createElement('canvas');
            return !!(elem.getContext && elem.getContext('2d'));
        }
        
        function Boton(estilo, accion) {
            this.r = canvasControles.width*0.05;
            this.x = this.r+Math.random()*(canvasControles.width-this.r);
            this.y = this.r+Math.random()*(canvasControles.height-this.r);
            this.vx = (Math.random()-0.5)*4;
            this.vy = (Math.random()-0.5)*4;
            this.accion = accion;
            this.estilo = estilo;
        }
        
        Boton.prototype = {
            mover: function() {
                    this.x += this.vx;
                    if (this.x+this.r > canvasControles.width) {
                            this.vx = -this.vx;
                            this.x = canvasControles.width-this.r;
                    } else if (this.x-this.r < 0) {
                            this.vx = -this.vx;
                            this.x = this.r;
                    }
                    this.y += this.vy;
                    if (this.y+this.r > canvasControles.height) {
                        this.vy = -this.vy;
                        this.y = canvasControles.height-this.r;
                    } else if (this.y-this.r < 0) {
                            this.vy = -this.vy;
                            this.y = this.r;
                    }
            }
        }
        
        
        function playPause() {
            if (video.paused) {
                video.play();
            } else {
                video.pause();
            }
        }
        
        function pintarPlay(ctx) {
            // Círculo de fondo
            ctx.beginPath();
            ctx.arc(this.x, this.y, this.r, 0, Math.PI*2);
            ctx.fillStyle = "rgba(220, 150, 190, 0.3)";
            ctx.closePath();
            ctx.fill();
            // Dibujo
            ctx.beginPath();
            if (video.paused) {
                ctx.moveTo(this.x-this.r/2, this.y-this.r/2);
                ctx.lineTo(this.x-this.r/2, this.y+this.r/2);
                ctx.lineTo(this.x+this.r/2, this.y);
                ctx.lineTo(this.x-this.r/2, this.y-this.r/2);
            } else {
                ctx.moveTo(this.x-this.r/2, this.y-this.r/2);
                ctx.lineTo(this.x-this.r/2, this.y+this.r/2);
                ctx.lineTo(this.x-this.r*0.2, this.y+this.r/2);
                ctx.lineTo(this.x-this.r*0.2, this.y-this.r/2);
                ctx.lineTo(this.x-this.r/2, this.y-this.r/2);
       
                ctx.moveTo(this.x+this.r/2, this.y-this.r/2);
                ctx.lineTo(this.x+this.r/2, this.y+this.r/2);
                ctx.lineTo(this.x+this.r*0.2, this.y+this.r/2);
                ctx.lineTo(this.x+this.r*0.2, this.y-this.r/2);
                ctx.lineTo(this.x+this.r/2, this.y-this.r/2);
            }
            ctx.fillStyle = "rgba(255, 25, 55, 0.3)";
            ctx.closePath();
            ctx.fill();
        }
        
        
        function muted() {
           video.muted = !video.muted;
        }
        
        function pintarMute(ctx) {
            // Círculo de fondo
            ctx.beginPath();
            ctx.arc(this.x, this.y, this.r, 0, Math.PI*2);
            ctx.fillStyle = "rgba(200, 120, 230, 0.3)";
            ctx.closePath();
            ctx.fill();
            // Altavoz
            ctx.beginPath();
            ctx.moveTo(this.x-this.r/2, this.y-this.r/4);
            ctx.lineTo(this.x-this.r/2, this.y+this.r/4);
            ctx.lineTo(this.x, this.y+this.r/4);
            ctx.lineTo(this.x+this.r/2, this.y+this.r/2);
            ctx.lineTo(this.x+this.r/2, this.y-this.r/2);
            ctx.lineTo(this.x, this.y-this.r/4);
            ctx.lineTo(this.x-this.r/2, this.y-this.r/4);
            ctx.fillStyle = "rgba(55, 255, 25, 0.3)";
            ctx.closePath();
            ctx.fill();
            // Silencio o no
            if (video.muted) {
                ctx.beginPath();
                ctx.moveTo(this.x-this.r/2, this.y-this.r/2);
                ctx.lineTo(this.x+this.r/2, this.y+this.r/2);
                ctx.strokeStyle = "rgba(255, 50, 50, 0.3)";
                ctx.closePath();
                ctx.stroke();
            }
        }

        
        function accionBn() {
           canvasVideo.classList.toggle("bn");
        }
        
        function pintarBn(ctx) {
            // Círculo de fondo
            ctx.beginPath();
            ctx.arc(this.x, this.y, this.r, Math.PI, Math.PI*2);
            ctx.fillStyle = "rgba(110, 210, 40, 0.3)";
            ctx.closePath();
            ctx.fill();
            // Altavoz
            ctx.beginPath();
            ctx.arc(this.x, this.y, this.r, 0, Math.PI);
            ctx.fillStyle = "rgba(5, 55, 75, 0.3)";
            ctx.closePath();
            ctx.fill();
        }
         function accionCd() {
           canvasVideo.classList.toggle("cd");
        }
       
        
        function pintarCd(ctx) {
            ctx.beginPath();
            ctx.arc(this.x, this.y, this.r, Math.PI, Math.PI*2);
            ctx.fillStyle = "rgba(110, 210, 40, 0.3)";
            ctx.closePath();
            ctx.fill();
            ctx.beginPath();
            ctx.arc(this.x, this.y, this.r, 0, Math.PI);
            ctx.fillStyle = "rgba(255, 55, 75, 0.3)";
            ctx.closePath();
            ctx.fill();
        }
        
        function accionBm() {
           canvasVideo.classList.toggle("bm");
        }
        
        function pintarBm(ctx) {
            // Círculo de fondo
            ctx.beginPath();
            ctx.arc(this.x, this.y, this.r, Math.PI, Math.PI*2);
            ctx.fillStyle = "rgba(110, 210, 40, 0.3)";
            ctx.closePath();
            ctx.fill();
            // Altavoz
            ctx.beginPath();
            ctx.arc(this.x, this.y, this.r, 0, Math.PI);
            ctx.fillStyle = "rgba(255, 255, 255, 0.3)";
            ctx.closePath();
            ctx.fill();
        }
         function accionBm() {
           canvasVideo.classList.toggle("gr");
        }
         function accionGr() {
           canvasVideo.classList.toggle("bm");
        }
        function pintarGr(ctx) {
            // Círculo de fondo
            ctx.beginPath();
            ctx.arc(this.x, this.y, this.r, Math.PI, Math.PI*2);
            ctx.fillStyle = "rgba(110, 210, 40, 0.3)";
            ctx.closePath();
            ctx.fill();
            // Altavoz
            ctx.beginPath();
            ctx.arc(this.x, this.y, this.r, 0, Math.PI);
            ctx.fillStyle = "rgba(25, 55, 255, 0.3)";
            ctx.closePath();
            ctx.fill();
        }
                function accionAn() {
           canvasVideo.classList.toggle("an");
        }
        
        function pintarAn(ctx) {
            // Círculo de fondo
            ctx.beginPath();
            ctx.arc(this.x, this.y, this.r, Math.PI, Math.PI*2);
            ctx.fillStyle = "rgba(110, 210, 40, 0.3)";
            ctx.closePath();
            ctx.fill();
            // Altavoz
            ctx.beginPath();
            ctx.arc(this.x, this.y, this.r, 0, Math.PI);
            ctx.fillStyle = "rgba(245, 154, 178, 0.3)";
            ctx.closePath();
            ctx.fill();
        }
      function accionFi() {
           canvasVideo.classList.toggle("fi");
        }
       
        
        function pintarFi(ctx) {
            ctx.beginPath();
            ctx.arc(this.x, this.y, this.r, Math.PI, Math.PI*2);
            ctx.fillStyle = "rgba(110, 210, 240, 0.23)";
            ctx.closePath();
            ctx.fill();
            ctx.beginPath();
            ctx.arc(this.x, this.y, this.r, 0, Math.PI);
            ctx.fillStyle = "rgba(255, 55, 75, 0.13)";
            ctx.closePath();
            ctx.fill();
        }
        